package com.kaltiot.smartsample;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ApplicationTest
{
    @Rule
    public ActivityTestRule<SensorActivity> mActivityRule = new ActivityTestRule<>(
            SensorActivity.class);

    @Before
    public void waitForOnline() throws InterruptedException {
        Thread.sleep(5000);
    }

    @Test
    public void testIsOnline(){
        onView(withId(R.id.connectiontext)).check(matches(withText("online")));
    }
}

