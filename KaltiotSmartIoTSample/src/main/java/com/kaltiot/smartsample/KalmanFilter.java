package com.kaltiot.smartsample;

public class KalmanFilter {
    private final float MinAccuracy = 1;

    private float Q_m_per_s = 3; // default value
    private long timestamp_ms;
    private double lat;
    private double lon;
    private float variance = -1; // -1 for uninitialized

    public KalmanFilter() {
        variance = -1; 
    }

    public void setQ(float Q) { Q_m_per_s = Q; }
    public long getTimestamp() { return timestamp_ms; }
    public float getLat() { return (float)lat; }
    public float getLon() { return (float)lon; }
    public float getAccuracy() { return (float)Math.sqrt(variance); }

    // Kalman filtering for location
    public void update(double lat_received, double lon_received, 
                       float accuracy, long timestamp) {

        if (accuracy < MinAccuracy) accuracy = MinAccuracy;

        if (variance < 0) {
            // if variance < 0, object is unitialised, so initialise with current values
            timestamp_ms = timestamp;
            lat = lat_received; 
            lon = lon_received; 
            variance = accuracy*accuracy; 
        } else {
            // else apply Kalman filter methodology
            long timeinc_ms = timestamp - timestamp_ms;

            if (timeinc_ms > 0) {
                // uncertainty of the current position increases as time goes on
                variance += timeinc_ms * Q_m_per_s * Q_m_per_s / 1000;
                timestamp_ms = timestamp;
            }

            // Kalman gain matrix K = Covariance * Inverse(Covariance + MeasurementVariance)
            float K = variance / (variance + accuracy * accuracy);
            lat += K * (lat_received - lat);
            lon += K * (lon_received - lon);

            // new Covarariance  matrix is (IdentityMatrix - K) * Covariance 
            variance = (1 - K) * variance;
        }
    }
}

