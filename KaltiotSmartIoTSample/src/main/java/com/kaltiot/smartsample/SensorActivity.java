package com.kaltiot.smartsample;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Switch;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.util.Log;
import android.content.res.Configuration;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.method.ScrollingMovementMethod;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.graphics.Bitmap;

import java.util.Calendar;
import java.util.Scanner;
import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

/* SensorActivity is connected to SensorService, receives data, state, location, etc.
   information from SensorService and displays it. */
public class SensorActivity extends Activity implements SensorEventListener, OnTouchListener {

    public static final String TAG = "Sensor";
    private static final long MY_PERMISSION_ACCESS_FINE_LOCATION = 111;

    View mFullView;			// For listening touch on whole display
    ImageView mImageView;		// For showing QR code
    TextView mTextInfo, mTextConnection, mTextRid, mTextValue;
    EditText mTextNotification;		// Scrollable text view
    Switch mGpsSwitch;			// Switch for GPS on/off
    String mGoogleAccount = "AndroidSensorCustomerId";  // Customer ID for this application

    private SensorServiceApi sensorService = null;	// Connection to SensorService
    private int mMaxNotificationLines = 50; // Text lines to show in mTextNotification
    private BufferedWriter mFileWriter = null;
    private String mAppId = null;

    // Handling Android sensor inputs
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mGyroscope;

    // Storing the sensor values
    private long mGyroTimestamp = 0;
    private long mPublishTimestamp = 0;
    private float mGyroX, mGyroY, mGyroZ;
    private float mGyroLimit = 10.0f;
    private long mAcclTimestamp = 0;
    private float mAcclX, mAcclY, mAcclZ, mAcclMax;
    private float mTouchX = 0.0f, mTouchY = 0.0f;
    private String mPublishStr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);
        
        mFullView = (View) findViewById(R.id.fullView);
        mFullView.setOnTouchListener(this);

        mTextInfo = (TextView) findViewById(R.id.infotext);
        getGoogleAccount();
        mTextInfo.append("Android: "+Build.VERSION.RELEASE);
        mTextConnection = (TextView) findViewById(R.id.connectiontext);
        mTextRid = (TextView) findViewById(R.id.ridtext);
        mTextValue = (TextView) findViewById(R.id.valuetext);
        mImageView = (ImageView) findViewById(R.id.imageview);
        mTextNotification = (EditText) findViewById(R.id.notificationtext);
        mTextNotification.setMovementMethod(new ScrollingMovementMethod());
        mTextConnection.setText("offline");
        mTextValue.setText(getCurrentTimeString());
        mGpsSwitch = (Switch) findViewById(R.id.gpsswitch);
        mGpsSwitch.setOnCheckedChangeListener(new GpsChangeListener());

        // Address and id for this app
        sensorService = new SensorServiceApi(TAG, mGoogleAccount, this, this);

        // Start service so it will run indefinitely
        Intent i = new Intent();
        i.setClassName("com.kaltiot.smartsample", "com.kaltiot.smartsample.SensorService");
        startService(i);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        updateQRCode("http://www.kaltiot.com");

        /* Checking for permissions at run time needed when targeting API 23 or later.
          if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                           new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
                           MY_PERMISSION_ACCESS_FINE_LOCATION);
        }*/

        sensorService.doBindService("com.kaltiot.smartsample", "com.kaltiot.smartsample.SensorService");
    }

    @Override
    protected void onResume() {
        super.onResume();
        int delay = SensorManager.SENSOR_DELAY_GAME;
        //int delay = SensorManager.SENSOR_DELAY_NORMAL;
        if (mGyroscope != null)
            mSensorManager.registerListener(this, mGyroscope, delay);
        if (mAccelerometer != null)
            mSensorManager.registerListener(this, mAccelerometer, delay);

        // Reconnect if Service connection has died while we were on background
        if (!sensorService.isServiceConnected())
            sensorService.doBindService("com.kaltiot.smartsample", "com.kaltiot.smartsample.SensorService");
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        if (isFinishing()) shutdown();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isFinishing()) shutdown();
    }

    @Override
    protected void onDestroy() {
        shutdown();
        try {
            mFileWriter.close();
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    private void shutdown() {
        sensorService.doUnbindService();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    // Receive Gyroscope and Accelerator events
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            mGyroTimestamp = event.timestamp;
            mGyroX = event.values[0];
            mGyroY = event.values[1];
            mGyroZ = event.values[2];
            // Saturate to given range
            if (mGyroX >  mGyroLimit) mGyroX =  mGyroLimit;
            if (mGyroX < -mGyroLimit) mGyroX = -mGyroLimit;
            if (mGyroY >  mGyroLimit) mGyroY =  mGyroLimit;
            if (mGyroY < -mGyroLimit) mGyroY = -mGyroLimit;
            if (mGyroZ >  mGyroLimit) mGyroZ =  mGyroLimit;
            if (mGyroZ < -mGyroLimit) mGyroZ = -mGyroLimit;
        } else if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            mAcclTimestamp = event.timestamp;
            mAcclX = event.values[0];
            mAcclY = event.values[1];
            mAcclZ = event.values[2];
            // Record the maximum acceleration value received 
            if (Math.abs(mAcclX) > mAcclMax) mAcclMax = Math.abs(mAcclX);
            if (Math.abs(mAcclY) > mAcclMax) mAcclMax = Math.abs(mAcclY);
            if (Math.abs(mAcclZ) > mAcclMax) mAcclMax = Math.abs(mAcclZ);
        }

        // Publish all sensor data with max frequency, skip duplicates
        if (event.timestamp > mPublishTimestamp + 30*1000) {
            mPublishTimestamp = event.timestamp;
            publish();
        }

    }

    public void publish() {
        String text=(int)mGyroX+","+(int)mGyroY+","+(int)mGyroZ+","+
                    (int)mAcclX+","+(int)mAcclY+","+(int)mAcclZ+","+
                    mTouchX+","+mTouchY;
        if (!text.equals(mPublishStr))
            sensorService.publishMessage(text, SensorServiceApi.PAYLOAD_STRING);
        mPublishStr = text;
    }

    // Receive touch events, store the latest touch point 
    public boolean onTouch(View v, MotionEvent ev) {
        int pointerCount = ev.getPointerCount();
        if (pointerCount == 0) return false;
        mTouchX = ev.getX(pointerCount-1)/v.getWidth();
        mTouchY = ev.getY(pointerCount-1)/v.getHeight();
        publish();
        return true;
    }

    protected void getGoogleAccount() {
        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
        Account[] list = manager.getAccountsByType("com.google");
        if (list.length > 0) {
            mGoogleAccount = list[0].name;
        }
        mTextInfo.append("Customer ID: " + mGoogleAccount + "\n");
    }

    public String getCurrentTimeString() {
        Calendar c = Calendar.getInstance(); 
        return c.get(Calendar.HOUR_OF_DAY)+"."+
               c.get(Calendar.MINUTE)+":"+
               c.get(Calendar.SECOND);
    }

    public String getCurrentTimeDateString() {
        Calendar c = Calendar.getInstance();
        return "time "+c.get(Calendar.HOUR_OF_DAY)+"."+
               c.get(Calendar.MINUTE)+":"+
               c.get(Calendar.SECOND)+" date "+
               c.get(Calendar.DATE)+"."+
               (c.get(Calendar.MONTH)+1)+".";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sensor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Encodes given string as QR code and updates mImageView
    public void updateQRCode(String qrString) {
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrString, null,
            Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(), 250);

        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            mImageView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    // Callback methods from SensorServiceApi
    public void state_callback(final String state) {
        runOnUiThread(new Runnable() {
            public void run() {
                mTextConnection.setText(state);
            }
        });
    }

    // Receive a unique Resource ID for this sensor
    public void rid_callback(final String rid) {
        runOnUiThread(new Runnable() {
            public void run() {
                if (mAppId == null)
                    mTextRid.setText("Device RID "+rid);
                else
                    mTextRid.setText("Application ID "+mAppId+"\nDevice RID "+rid);
                updateQRCode("https://demo.e2e.kaltiot.com/gyro?rid="+rid);
            }
        });
    }

    // Receive the application ID for this service
    public void appid_callback(String appid) {
        mAppId = appid;
    }

    // Updates the UI text field
    public void value_callback(final String value) {
        runOnUiThread(new Runnable() {
            public void run() {
              mTextValue.setText(value);
            }
        });
    }

    public void addNotificationText(final String str) {
        runOnUiThread(new Runnable() {
            public void run() {
                /* Adding text to notification view, remove the
                   oldest lines to limit the amount of lines. */
                if (mTextNotification != null && mTextNotification.getLayout() != null &&
                    mTextNotification.getText() != null) {
		    String oldtext = mTextNotification.getText().toString();
		    String newtext = "\n" + getCurrentTimeString() + ": " + str;
		    if (mTextNotification.getLineCount() > mMaxNotificationLines) {
			int removeLines = mTextNotification.getLineCount() - mMaxNotificationLines + 1;
			int removeChars = mTextNotification.getLayout().getLineEnd(removeLines);
			if (removeChars < oldtext.length())
			    oldtext = oldtext.substring(removeChars, oldtext.length());
                    }
		    mTextNotification.setText(oldtext + newtext);
                }
            }
        });
    }

    // Receiving notifications, display on UI and parse gps=0 message to update GPS switch
    public void notification_callback(String address, String str, int payload_length,
                int payload_type, String msg_id) {

        /* Update notification text view */
        addNotificationText(str);

        if (str == null) return;

	/* Parse gps= from the notification message and update UI accordingly,
           the location listener is in SensorService */
	if (str.length() > 4 && str.substring(0,4).equals("gps=")) {
            if (str.charAt(4) == '0')
                mGpsSwitch.setChecked(false);
            else
                mGpsSwitch.setChecked(true);
        }
    }
    
    // Listener for GPS switch button, send message to SensorService
    public class GpsChangeListener implements CompoundButton.OnCheckedChangeListener {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	    Log.i(TAG, "GPS " + (isChecked ? "on" : "off"));
            sensorService.setGps(isChecked);
        }
    }

}
