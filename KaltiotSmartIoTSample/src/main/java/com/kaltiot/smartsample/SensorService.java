package com.kaltiot.smartsample;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.app.Service;
import android.app.PendingIntent;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.os.Messenger;
import android.os.Message;
import android.os.Handler;
import android.os.IBinder;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.RemoteException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.location.Location;
import android.util.Log;
import android.content.res.Configuration;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

// We use KaltiotSmartGatewayApi to connect to KaltiotSmartGateway
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;
import com.kaltiot.smartgatewayapi.KaltiotSmartGatewayApi;
import com.kaltiot.smartgatewayapi.KaltiotSmartGatewayApiCallbacks;

/* SensorService runs on background, it
   - is connected to KaltiotSmartGateway through KaltiotSmartGatewayApi,
   - listens to location updates and publishes location
   - receives notifications and interprets following commands from notifications:
     * notification="Sender Message", displays an Android pop-up notification
     * interval=xxx, sets the publish interval in seconds
     * gps=0/1, enables or disables GPS
   - is connected to SensorActivity through Handler/Messenger,
   - pushes stuff that needs to be shown on UI to SensorActivity. */
public class SensorService extends Service implements
        KaltiotSmartGatewayApiCallbacks,
        DataApi.DataListener,
        ConnectionCallbacks,
        OnConnectionFailedListener {

    private static final String TAG = "SensorService";
    private static final String HEARTRATE_KEY = "com.kaltiot.heart.rate";

    // Kaltiot Android Service running in the background, added as library dependency, will run
    // in this application context
    String servicePackage = "com.kaltiot.smartsample";
    KaltiotSmartGatewayApi ksg = null;	// API for the Service

    // Google Account name used as customer_id for this sensor
    String mGoogleAccount = "android_sensor";

    // Current connection info received from the Service
    int mState = 0;
    String mRid = "";
    String mAppid = "";

    // The sensor UI is client for this service
    Messenger mClients = null;

    // The stuff we publish from this sensor
    int mTimerIntervalSeconds = 30; // Initial interval for location publish
    String mLatLon = "0.0,0.0";
    String mSensorString = "0,0,0,0,0,0";
    // The text we want to display on the UI
    String mPublishText = "";
    String mLatText = "";
    String mLonText = "";
    int mHeartrate = 0;

    // Android specific variables for handling the sensor inputs and alarms
    private boolean mServicePackageAvailable = true;
    private boolean mGpsEnabled = false;
    private Long mLocationRequestTime = 0L;
    private Long mMaxGpsEnableSeconds = 2*60*60L; // Switch off GPS after a while to save battery
    private int mNotificationId = 001;
    final LocationReceiver mLocationReceiver = new LocationReceiver();
    public static final String ACTION_LOCATION = "com.kaltiot.SensorLocation";
    private PendingIntent mLocationIntent = null;
    private BufferedWriter mFileWriter = null;
    private KalmanFilter mLocFilter = new KalmanFilter();
    private Location mLastLocation;
    protected GoogleApiClient mGoogleApiClient;


    @Override
    public void onCreate() {
        getGoogleAccount();
        buildGoogleApiClient();
        mGoogleApiClient.connect();

        // Address, customer_id and capabilities for this sensor app
        String[] capabilities = { "AndroidSensorSample" };
        ksg = new KaltiotSmartGatewayApi(servicePackage, TAG, mGoogleAccount, capabilities, null, this, this);

        registerReceiver(mLocationReceiver, new IntentFilter(ACTION_LOCATION));

        File logFile = new File(getExternalFilesDir(null), "sensor_locations.txt");
        Log.i(TAG, "Log file "+logFile.getAbsolutePath());
        /* File logging for locations
        try {
            mFileWriter = new BufferedWriter(new FileWriter(logFile));
        } catch (Exception e) {
            Log.i(TAG, "Log file failed");
        }*/

        // Increase our priority to prevent from getting killed when running on background
        Thread thr = getMainLooper().getThread();
        Log.i(TAG, "Priority="+thr.getPriority());
        if (thr.getPriority() < 8) thr.setPriority(thr.getPriority()+2);
 
        try {
            PackageManager mgr = getPackageManager();
            PackageInfo info = mgr.getPackageInfo(servicePackage, 0);
            // Check info.versionCode if depending on service version
        } catch (Exception e) {
            mServicePackageAvailable = false;
            Log.i(TAG, "Service package not available");
            e.printStackTrace();
            return;
        }

        // Start service so it will run indefinitely on the background
        ksg.startService();

        // Bind to MqService to register and receive notifications
        ksg.doBindService();
    }

    @Override
    public void onDestroy() {
        shutdown();
        try {
            if (mFileWriter != null) mFileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        unregisterReceiver(mLocationReceiver);
    }

    private void shutdown() {
        stopLocationUpdates();
        Wearable.DataApi.removeListener(mGoogleApiClient, this);
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        ksg.doUnbindService();
    }

    /**
     * Allow starting the service so that it keeps running on the background even 
     * if the sensor UI is closed. This is to receive location information on background.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    // Using GoogleApiClient for GPS location updates
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Wearable.API)
                .build();
    }

    protected void stopLocationUpdates() {
        if (mLocationIntent != null && mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, mLocationIntent);
        mLocationIntent = null;
    }

    // Use the google account to identify this sensor
    protected void getGoogleAccount() {
        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
        Account[] list = manager.getAccountsByType("com.google");
        if (list.length > 0)
        {
            mGoogleAccount = list[0].name;
        }
    }

    // Runs when a GoogleApiClient object successfully connects.
    @Override
    public void onConnected(Bundle connectionHint) {
        // Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            updateLocation(mLastLocation);
        }
        requestLocationUpdates();
        Wearable.DataApi.addListener(mGoogleApiClient, this);
    }

    private void requestLocationUpdates() {
        int prio = mGpsEnabled ? LocationRequest.PRIORITY_HIGH_ACCURACY :
                                 LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
        LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(mTimerIntervalSeconds*1000);
            mLocationRequest.setFastestInterval(5*1000);
            mLocationRequest.setPriority(prio);

        // Request location updates as broadcast intents
        if (mLocationIntent == null) {
            Intent intent = new Intent(ACTION_LOCATION);
            mLocationIntent = PendingIntent.getBroadcast(getApplicationContext(),
                        0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        }
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, mLocationIntent);
            mLocationRequestTime = System.currentTimeMillis()/1000;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        switch (result.getErrorCode()) {
            case ConnectionResult.SERVICE_MISSING:
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
            case ConnectionResult.SERVICE_DISABLED:
                Log.i(TAG, "Google Play Service unavailable.");
                break;
            default:
                Log.i(TAG, "Google Play Service Connection failed: ErrorCode = "+
                      result.getErrorCode());
        }
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Google Play Service Connection suspended.");
        mGoogleApiClient.connect();
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            if (mLastLocation != null) {
               long timeFromLast=(location.getTime()-mLastLocation.getTime())/1000;
               timeFromLast++;
            }
            updateLocation(location);
            mLastLocation = location;
        }
        //Log.i(TAG, "Location update received at " + getCurrentTimeString());
    }

    public void updateLocation(Location location) {
        if (location == null) return;
        // Location from Kalman filter
        float Q = 3;
        if (location.getSpeed() > Q) Q = location.getSpeed();
        mLocFilter.setQ(Q);
        mLocFilter.update(location.getLatitude(), location.getLongitude(),
			  location.getAccuracy(), location.getTime());
        String lat = String.valueOf(location.getLatitude());
        String lon = String.valueOf(location.getLongitude());
        String latf = String.valueOf(mLocFilter.getLat());
        String lonf = String.valueOf(mLocFilter.getLon());

        mLatText = "Lat: " + latf + "  update "+getCurrentTimeString();
        mLonText = "Lon: "+ lonf + "  acc: "+String.valueOf(mLocFilter.getAccuracy())+"m";

        Log.i(TAG, "Location: "+lat+","+lon+
                   "  accuracy: "+String.valueOf(location.getAccuracy())+
                   "m  Filtered: "+latf+","+lonf+
                   "  accuracy: "+String.valueOf(mLocFilter.getAccuracy()));
        mLatLon = latf+","+lonf;

        // Possibility to log the locations in a file
        if (mLastLocation != null)
            logFileLine(getCurrentTimeString()+" "+mLatLon+
                "  accuracy: "+String.valueOf(location.getAccuracy())+
                "m  provider: "+location.getProvider()+
                "  speed="+String.valueOf(location.getSpeed())+
                "  distanceToLast="+String.valueOf(location.distanceTo(mLastLocation))+
                "  timeFromLast="+String.valueOf(location.getTime()-mLastLocation.getTime()));
    }


    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                // DataItem changed
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo("/heartrate") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    mHeartrate = dataMap.getInt(HEARTRATE_KEY);
                    Log.d(TAG, "Heartrate data: " + mHeartrate);
                    publishAndUpdate();
                }
            } else if (event.getType() == DataEvent.TYPE_DELETED) {
                // DataItem deleted
            }
        }
    }

    public void publishAndUpdate() {
        // Publish location and sensor data
        String publish = mLatLon+","+mHeartrate+","+mSensorString;
        ksg.publish(publish.getBytes(), KaltiotSmartGatewayApi.PAYLOAD_STRING);
        // Update UI
        mPublishText = "Publish " + publish + " at " + getCurrentTimeString();
        Log.i(TAG, mPublishText);
        Message msg = createUIUpdateMessage(mPublishText, mLatText, mLonText);
        sendMessage(msg);
    }

    // File logging
    public void logFile(String str) {
        try {
            if (mFileWriter != null) mFileWriter.write(str, 0, str.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void logFileLine(String str) {
        if (mFileWriter == null) return;
        try {
            mFileWriter.write(str, 0, str.length());
            mFileWriter.newLine();
            mFileWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getCurrentTimeString() {
        Calendar c = Calendar.getInstance(); 
        return c.get(Calendar.HOUR_OF_DAY)+"."+
               c.get(Calendar.MINUTE)+":"+
               c.get(Calendar.SECOND);
    }

    // Create an Android pop-up notification with intent to launch this activity
    private void showNotification(String message) {
        NotificationManager mNotifyMgr = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_mq_noti);
        Intent i = new Intent();
        i.setClassName("com.kaltiot.smartsample", "com.kaltiot.smartsample.SensorActivity");
        Intent resultIntent = i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true);
        // Issue a notification
        String[] str = message.split("\\s");
        if (str.length > 0) {
            mBuilder.setContentTitle(str[0]);
            if (message.length() > str[0].length()) {
                mBuilder.setContentText(message.substring(str[0].length() + 1, message.length()));
            }
        } else {
            mBuilder.setContentTitle(message);
        }
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    /* Callback methods from KaltiotSmartGatewayApiCallbacks interface */
    public void state_callback(int state, int error) {
        mState = state;
        // Update the UI
        Message msg = createMessage("", getConnectionState(), SensorServiceApi.MSG_SET_STATE);
        sendMessage(msg);
    }

    public boolean isOnline() {
        if (mState == 3)
            return true;
        else
            return false;
    }

    public String getConnectionState() {
        if (mState == 3)
            return "online";
        if (mState == 2)
            return "offline";
        return "no network";
    }

    public void rid_callback(String address, final String rid) {
        // Pass through to UI
        mRid = rid;
        Message msg = createMessage(address, rid, SensorServiceApi.MSG_SET_RID);
        sendMessage(msg);
    }

    public void appid_callback(String address, final String appid) {
        // Pass through to UI
        mAppid = appid;
        Message msg = createMessage(address, appid, SensorServiceApi.MSG_SET_APPID);
        sendMessage(msg);
    }

    public void notification_callback(String address, byte[] payload,
                int payload_type, String msg_id) {
        // In this sample we handle the payload always as String
        String str = new String(payload);
        //Log.i(TAG, "Received notification payload_type="+payload_type+" length="+payload.length+" : "+str);
        // Pass the payload through to UI
        Message msg = createMessage(address, str, SensorServiceApi.MSG_SEND_PAYLOAD);
        msg.arg1 = payload_type;
        sendMessage(msg);

        // Respond to ping
        if (payload_type == KaltiotSmartGatewayApi.PAYLOAD_PING && address.equals(TAG)) {
            String publish = "sensor_pong";
            ksg.publish(publish.getBytes(), KaltiotSmartGatewayApi.PAYLOAD_PONG);
            // Update UI
            mPublishText = "Publish " + publish + " at " + getCurrentTimeString();
            msg = createUIUpdateMessage(mPublishText, mLatText, mLonText);
            sendMessage(msg);
            return;
        }

        if (payload_type != KaltiotSmartGatewayApi.PAYLOAD_STRING)
            Log.i(TAG, "Received notification payload_type="+payload_type+" length="+payload.length+" : "+str);

        if (str == null) return;

        /* Parse interval=seconds from the notification message */
        if (str.length() > 9 && str.substring(0,9).equals("interval=")) {
            Scanner scanner = new Scanner (str.substring(9));
            if (!scanner.hasNextInt()) return;
            int interval = scanner.nextInt ();
            if (interval > 0) {
                mTimerIntervalSeconds=interval;
                // New timer interval received, re-schedule location updates
                requestLocationUpdates();
            }
        } else if (str.length() > 13 && str.substring(0,13).equals("notification=")) {
            /* Parse notification=message */
            String message = str.substring(13);
            showNotification(message);
        } else if (str.length() > 4 && str.substring(0,4).equals("gps=")) {
            /* Parse gps= message to set GPS */
            if (str.charAt(4) == '0')
                setGps(false);
            else
                setGps(true);
        }
    }

    // Receiver for location updates
    public class LocationReceiver
               extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // Receiver for ACTION_LOCATION, receives location updates also on background
            if (intent != null) {
                if (LocationResult.hasResult(intent)) {
                    Location location = (Location) LocationResult.extractResult(intent).getLastLocation();
                    onLocationChanged(location);
                    // Update coordinates to UI
                    Message msg = createUIUpdateMessage(mPublishText, mLatText, mLonText);
                    sendMessage(msg);
                }
            }
            // Make sure we're connected to service and publish the new location
            if (!mServicePackageAvailable) {
                mPublishText = "KaltiotSmartGateway not available";
                mLatText = "Install APK and try again";
                mLonText = "";
                Message msg = createUIUpdateMessage(mPublishText, mLatText, mLonText);
                sendMessage(msg);
            } else if (!ksg.isServiceConnected()) {
                Log.i(TAG, "No service connected, restarting");
                mPublishText = "KaltiotSmartGateway not connected";
                mLatText = "Attempting reconnect";
                mLonText = "";
                Message msg = createUIUpdateMessage(mPublishText, mLatText, mLonText);
                sendMessage(msg);
                ksg.doBindService();
            } else if (isOnline()) {
                // Publish location and sensor data to cloud and update UI
                publishAndUpdate();
                Message msg = createMessage(TAG, getConnectionState(), SensorServiceApi.MSG_SET_STATE);
                sendMessage(msg);
                msg = createMessage(TAG, mRid, SensorServiceApi.MSG_SET_RID);
                sendMessage(msg);

            }
            Long currTime = System.currentTimeMillis()/1000;
            if (mGpsEnabled && currTime > mLocationRequestTime + mMaxGpsEnableSeconds) {
                // Switch off GPS after a while, also update UI
                setGps(false);
                Message msg = createMessage("Sensor", "gps=0", SensorServiceApi.MSG_SEND_PAYLOAD);
                msg.arg1 = SensorServiceApi.PAYLOAD_STRING;
                sendMessage(msg);
            }
        }
    }

    // Action when new sensor is registered
    void handleSensorRegister() {
        // send state/RID/UI to newly registered client
        // in case KaltiotSmartGateway was already connected we don't receive callbacks
        Message message = createMessage("", getConnectionState(), SensorServiceApi.MSG_SET_STATE);
        sendMessage(message);
        message = createMessage("", mRid, SensorServiceApi.MSG_SET_RID);
        sendMessage(message);
        message = createUIUpdateMessage(mPublishText, mLatText, mLonText);
        sendMessage(message);
    }

    // Action when sensor wants to publish a message
    void handleSensorMessage(int arg1, String str) {
        if (arg1 == SensorServiceApi.PAYLOAD_STRING) {
            mSensorString = str;
            // Publish location and sensor data
            String publish = mLatLon+","+mHeartrate+","+mSensorString;
            ksg.publish(publish.getBytes(), KaltiotSmartGatewayApi.PAYLOAD_STRING);
            mPublishText = "Publish " + publish + " at " + getCurrentTimeString();
        }
        // Update UI
        Log.i(TAG, mPublishText);
        Message message = createUIUpdateMessage(mPublishText, mLatText, mLonText);
        sendMessage(message);
    }

    /**
     * Handler of incoming messages from client (SensorActivity).
     */
    class ServiceIncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SensorServiceApi.MSG_REGISTER_CLIENT:
                    mClients = msg.replyTo;
                    String address = msg.getData().getString("address");
                    String version = msg.getData().getString("version");
                    String id = msg.getData().getString("id");
                    Log.i(TAG, "Sensor registered "+mClients );
                    handleSensorRegister();
                    break;
                case SensorServiceApi.MSG_UNREGISTER_CLIENT:
                    mClients = null;
                    address = msg.getData().getString("address");
                    version = msg.getData().getString("version");
                    id = msg.getData().getString("id");
                    Log.i(TAG, "Sensor unregistered");
                    break;
                case SensorServiceApi.MSG_SEND_PAYLOAD:
                    String str = msg.getData().getString("data");
                    if (isOnline()) {
                        handleSensorMessage(msg.arg1, str);
                    }
                    break;
                case SensorServiceApi.MSG_SET_GPS:
                    Log.i(TAG, "Set gps: "+msg.arg1);
                    setGps(msg.arg1 == 0 ? false : true);
                    break;
                 default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Target we publish for clients to send messages to ServiceIncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new ServiceIncomingHandler());

    /**
     * When client is binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "Binding to client");
        return mMessenger.getBinder();
    }

    // Create a message to send to a client
    private Message createMessage(String address, String data, int what) {
        // Send the string as data in bundle
        Bundle b = new Bundle();
        b.putString("data", data);
        b.putString("address", address);
        Message msg = Message.obtain(null, what);
        msg.setData(b);
        return msg;
    }

    private Message createMessage(int value, int what) {
        // Send the value as msg.arg1
        Message msg = Message.obtain(null, what);
        msg.arg1 = value;
        return msg;
    }

    // Create a message for UI update
    private Message createUIUpdateMessage(String line1, String line2, String line3) {
        // Send the string as data in bundle
        Bundle b = new Bundle();
        b.putString("data", line1 + "\n" + line2 + "\n" + line3);
        b.putString("address", "");
        Message msg = Message.obtain(null, SensorServiceApi.MSG_SET_VALUE);
        msg.setData(b);
        return msg;
    }

    // Send a message to our client
    public void sendMessage(Message msg) {
        if (mClients != null) {
            try {
                mClients.send(msg);
            } catch (RemoteException e) {
                mClients = null;
            }
        }
    }

    // GPS set from either UI or received notification
    public void setGps(boolean enabled) {
        if (mGpsEnabled != enabled) {
            mGpsEnabled = enabled;
            requestLocationUpdates();
        }
    }
}
