# README #

Example Android application acting as a sensor device. Depends on the KaltiotSmartGateway Android Library. Connects to the KaltiotSmartGateway using KaltiotSmartGatewayApi and publishes sensor data. Registers for location updates and gyro/accelerometer sensor input. 

### How do I get set up? ###

* install android-studio
* install android sdk, API 16
* log into https://console.torqhub.io, go to Downloads-page, click "Android SDK" and download your personal Android SDK
* copy your Android SDK Library into android_sensor/KaltiotSmartGateway/KaltiotSmartGateway.aar 
* compile using gradle `./gradlew assembleDebug` or from android-studio IDE
* apk is created into KaltiotSmartIoTSample/build/outputs/apk/

### Adding Kaltiot Smart IoT to your existing Android application ###

All you need to do is download your Android SDK from https://console.torqhub.io and add the library to your project

* copy your Android SDK to YourAndroidApplication/KaltiotSmartGateway/KaltiotSmartGateway.aar
* edit YourAndroidApplication/settings.gradle and make sure it has
```
include ':KaltiotSmartGateway'
```
* edit YourAndroidApplication/YourApp/build.gradle and add following lines in dependencies section:
```
dependencies {
    compile project(':KaltiotSmartGateway')
    compile 'com.android.support:support-v4:22.2.0'
    compile 'com.google.protobuf:protobuf-java:2.6.1'
}
```
* That's it, then your code can use KaltiotSmartGatewayApi

### Structure ###

Java sources are located in src/main/java/com/kaltiot/smartsample. Project includes ZXing Java library for encoding QR code, located under libs. Application connects to com.kaltiot.smartgateway.KaltiotSmartGateway Android Service using KaltiotSmartGatewayApi interface. Example code is divided in two parts: SensorService and SensorActivity.

* SensorService runs on the background, binds to KaltiotSmartGateway, receives location updates and publishes data at fixed intervals. If it receives a notification from KaltiotSmartGateway, it will set up an Android Notification with intent to launch SensorActivity.
* SensorActivity is the UI, displaying QR code, status text and received notifications. Activity binds to SensorService. When the UI is active it receives gyro and accelerometer sensor data and publishes it.

The interface class SensorServiceApi wraps the Service and Messenger interface into simple function and callback interface similar to KaltiotSmartGatewayApi. KaltiotSmartGatewayApi is used by SensorService to connect to KaltiotSmartGateway and SensorServiceApi is used by SensorActivity to connect to SensorService.