#/bin/bash
# Script for running online test on docker

# Android emulator must be running, using adb to connect to it
pwd

# Rewriting local.properties for docker image
echo "Local.properties was:"
cat local.properties
echo "sdk.dir=/opt/android-sdk-linux" > local.properties
echo "Local.properties is:"
cat local.properties

# Check if ADB is connected to a emulator
state=`adb get-state`
echo State: $state

if [[ $state != "device" ]]
then
    echo "Android emulator not running"
    adb devices
    exit -1
fi

./gradlew clean

# Issue input event to unlock screen
adb shell input keyevent 82

# Build and run TorqSensor testIsOnline
./gradlew connectedAndroidTest
err=$?
echo Result $err
exit $err

